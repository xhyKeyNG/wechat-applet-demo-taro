export default {
  pages: [
    "pages/home/home",
    "pages/index/index",
    "pages/test/index",
    "pages/test/testVue",
    "pages/list/list",
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "Demo",
    navigationBarTextStyle: "black",
  },
};
