import { defineComponent, onMounted } from "vue";
import styles from "./test.less";

const TestPage = defineComponent((props) => {
  const handleToClick = () => {
    console.log(123);
  };

  onMounted(() => {
    console.log(234);
  });

  return () => {
    return (
      <view class={styles.testBtn} onTap={() => handleToClick()}>
        123123
      </view>
    );
  };
});

export default TestPage;
